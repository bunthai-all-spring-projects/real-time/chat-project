package chat.project.webflux.configurations.websocket

import io.netty.handler.timeout.IdleStateHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.server.RequestUpgradeStrategy
import org.springframework.web.reactive.socket.server.WebSocketService
import org.springframework.web.reactive.socket.server.support.HandshakeWebSocketService
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter
import org.springframework.web.reactive.socket.server.upgrade.ReactorNettyRequestUpgradeStrategy
import org.springframework.web.reactive.socket.server.upgrade.TomcatRequestUpgradeStrategy

@Configuration
class WebSocketConfiguration {

    @Bean
    fun handlerMapping(): HandlerMapping {
        val map = mapOf("/chat" to CustomWebSocketHandler())
        val order = -1 //Before annotated controllers
        return SimpleUrlHandlerMapping(map, order)
    }

    @Bean
    fun handlerAdapter(): WebSocketHandlerAdapter {
//        return WebSocketHandlerAdapter()
        return WebSocketHandlerAdapter(webSocketService())
    }

    @Bean
    fun webSocketService(): WebSocketService {
//        val strategy = TomcatRequestUpgradeStrategy().apply {
//            setMaxSessionIdleTimeout(0L)
//        }
        val strategy: RequestUpgradeStrategy = ReactorNettyRequestUpgradeStrategy()
        return HandshakeWebSocketService(strategy)
    }

}