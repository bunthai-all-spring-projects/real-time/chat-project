package chat.project.webflux.configurations.websocket

import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.concurrent.CompletableFuture
import java.util.function.Function

class CustomWebSocketHandler : WebSocketHandler {

    override fun handle(session: WebSocketSession): Mono<Void> {

        CompletableFuture.runAsync {
            while (true) {
                Thread.sleep(1000)

            }
        }

        val output = session.receive()
            .doOnNext {
//                println("doOnNext")
            }
            .doFinally {
                println("doFinally")
            }
            .map {
                println("Echo ${it.payloadAsText}")
//                session.textMessage("Echo $it")
                session.textMessage(it.payloadAsText)
            }
            .doOnSubscribe {
                println("ssssssssssssubscribe: ${session.isOpen}")
            }

        return session.send(output)

    }

}