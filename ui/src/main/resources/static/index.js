document.addEventListener('DOMContentLoaded', function(event) {

const name = document.getElementById("name");
name.value = Math.random().toString(36).substring(7);

const chat = document.getElementById("chat");
const send = document.getElementById("send");
const message = document.getElementById("message");

// Create WebSocket connection.
const socket = new WebSocket("ws://192.168.0.209:7000/chat");

// Connection opened
socket.addEventListener("open", function(event) {});

// Listen for messages
socket.addEventListener("message", function(event) {
  console.log("Message from server ", event.data);
  const r = document.createElement("tr");

  const payload = JSON.parse(event.data)

  r.appendChild(document.createTextNode(`${payload.name}:  ${payload.chat}`))
  message.appendChild(r)
});

send.addEventListener("click", function() {
  const payload = {
    name: name.value,
    chat: chat.value
  }
  chat.value = '';
  socket.send(JSON.stringify(payload));
});

});