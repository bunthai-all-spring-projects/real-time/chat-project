package chat.project.ui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/chat")
public class UIController {

    @GetMapping
    public ModelAndView chat() {
        return new ModelAndView("index.html");
    }


}
